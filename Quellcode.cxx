#include <iostream>
#include<vector>
#include<math.h>

int main()
{
	const int rows = 4;
	const int cols = 4;

	//Vierer_Vektoren am Pixeldetektor
	std::vector<std::vector<int>> my_vector(rows, std::vector<int>(cols, 0));

	my_vector[0] = { 1,2,3,4 };//(E, px,py,pz)
	my_vector[1] = { 2,1,1,2 };//
	my_vector[2] = { 1,1,2,1 };//
	my_vector[3] = { 0,4,3,0 };//

	//pT_Vektoren
	std::vector<std::vector<int>> my_pT_vector(rows, std::vector<int>(cols, 0));

	my_pT_vector[0] = { -1,2,6,3 };//(ET, pTx,pTy,pTz)
	my_pT_vector[1] = { 7,3,-6,2 };//
	my_pT_vector[2] = { 5,3,1,5 };//
	my_pT_vector[3] = { 0,4,3,0 };//

	for (int i = 1; i < my_vector.size(); ++i)
	{
		for (int j = 1; j < my_vector[i].size(); ++j)
		{
			std::cout << "My_Vector [" << i << "," << j << "]= " << my_vector[i][j] << std::endl;
		}
	}

	std::cout << std::endl;

	//MAtrixauswertung des ersten Impulsvektors
	std::vector<std::vector<int>> my_matrix(rows, std::vector<int>(cols, 0));

	for (int k = 0; k <= 3; k++)
	{
		for (int i = 0; i < my_matrix.size(); i++)
		{
			for (int j = 1; j < my_matrix[i].size(); j++)
			{
				my_matrix[i][j] = my_vector[k][i] * my_vector[k][j];

			}
		}
		for (int i = 0; i < my_matrix.size(); ++i)
		{
			for (int j = 0; j < my_matrix[i].size(); ++j)
			{
				std::cout << "My_Matrix [" << i << "," << j << "]= " << my_matrix[i][j] << std::endl;
			}
		}

		std::cout << std::endl;
	}




	//Berechnung der einzelnen pT-Betraege
	/*for(int i=0; i< my_pT_vector.size();i++)
	{
		for (int k = 0; k <= 3; ++k)
		{
			float sum=0.0;
			sum += my_pT_vector[i][k] * my_pT_vector[i][k];
			float ergebnis = sqrt(sum);
			std::cout << "Der Betrag des "<<k<<"ten pT-Vektors ist gleich " << ergebnis << " " << std::endl;
		}*/
}
